class Storage{
	get(key){}; //Must return a promise
	set(url, data){}; //Must return a promise
	delete(key){}
}

class LocalStorage extends Storage{
	
	get(key){
		return browser.storage.local.get(key); 
	};
	set(url, data){

		var reg = {};
			reg[url] = data;

		return browser.storage.local.set(reg); //{key: value});
	};
}

//There is a RemoteStorage class you can use but just from the background, so it was moved there