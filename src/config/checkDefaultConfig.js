var storage = new LocalStorage();
storage.get("config").then(function checkDefaultValues(result) {

  	if(result.config) return;

  	storage.set("config", {
	    "templates-repo-uri": "http://localhost:8080/logikos-api/templates"
	});
});