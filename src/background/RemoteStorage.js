class RemoteStorage extends Storage{
	
	get(key){
		
		return new Promise((resolve, reject) => {

	        browser.storage.local.get("config").then(function(result) {

				var templatesUri = result.config["templates-repo-uri"];
				key = encodeURIComponent(key);
			    var endpoint = `${templatesUri}/matching?url=${key}`;
			    
			    var req = new XMLHttpRequest();
				req.open('GET', endpoint, false); 
				req.send(null);
				
				var results = (req.responseText) ? JSON.parse(req.responseText) : [];
				resolve(results);
			});
	    });
	};
	set(url, data){

		return new Promise((resolve, reject) => {

	      browser.storage.local.get("config").then(function(result) {

	        var endpoint = result.config["templates-repo-uri"];

	        var req = new XMLHttpRequest();
	        req.open('PATCH', endpoint, false);
	        req.setRequestHeader("Content-type", "application/json");

	        console.log(data);

	        var postItemRequest = JSON.stringify({ 
	        	"id": data.id,
				"name": data.name, 
				"favIcon": data.favIcon, 
				"urlPattern": data.urlPattern, 
				"propertySelectors": data.propertySelectors, 
				"itemType": data.itemType, 
				"groups": [ 
				 "['public']" 
				], 
				"owner": "no_reply@lifia.info.unlp.edu.ar" 
			});

	        req.send(postItemRequest);
	        resolve();
	      });
	    });
	};
	delete(id){
		return new Promise((resolve, reject) => {

			browser.storage.local.get("config").then(function(options) {

				var endpoint = options.config["templates-repo-uri"] + "/" + id;
				console.log(endpoint);

		        var req = new XMLHttpRequest();
		        req.open('DELETE', endpoint, false);
		        req.setRequestHeader("Content-type", "application/json");

		        req.send();
		        resolve();
		    });
	    });
	};
}