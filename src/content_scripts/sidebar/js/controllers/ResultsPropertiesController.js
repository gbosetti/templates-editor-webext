serviceCreator.controller('ResultsPropertiesController', function($scope, $state, TemplateService) {

  AbstractController.call(this, $scope, $state, TemplateService);
  $scope.service = {
    propertySelectors: {}
  };
  $scope.loadDataModel = function() {
    TemplateService.getService().then(function(service) {

      $scope.service = service;
      $scope.loadPropertiesIntoSidebar($scope.service.propertySelectors);

      $scope.enableDomElementSelection($scope.getElementsSelector(), "onElementSelection", 
        ".well", "XpathScrapper", undefined, false, true).then(function() {

        $scope.highlightPropertiesInDom($scope.service.propertySelectors);
      });
    });
  };

  $scope.enableDomElementSelection = function(controlsSelector, callbackMessage, scoped, 
    scrapperClass, refElemSelector, removeStyleOnSelection, generateRelativeSelector) {
    removeStyleOnSelection = (removeStyleOnSelection == undefined)
      ? true
      : removeStyleOnSelection;

    return browser.runtime.sendMessage({
      call: "enableElementSelection",
      args: {
        targetElementSelector: controlsSelector,
        onElementSelection: callbackMessage,
        scoped: scoped,
        scrapperClass: scrapperClass || "QuerySelectorScrapper",
        refElemSelector: refElemSelector,
        removeStyleOnSelection: removeStyleOnSelection,
        generateRelativeSelector: generateRelativeSelector || false
      }
    });
  };

  $scope.loadPrevStep = function(aState) {
    if (this.areRequirementsMet()) {
      $scope.saveDataModel();
      $scope.undoActionsOnDom();
      $state.go(aState);
    }
  };

  $scope.undoActionsOnDom = function(aState) {

    var elemsSelector = $scope.getElementsSelector(); // $scope.service.results.selector.value);
    $scope.disableDomElementSelection(elemsSelector);
    $scope.removeFullSelectionStyle();
  };

  $scope.arePropertiesDefined = function() {
    var inputs = document.querySelectorAll("input");
    this.removeFormElementById("no_props_error");

    if (inputs == undefined || inputs.length <= 0) {
      this.showErrorMessageByElems("no_props_error", document.querySelector(".list-group"), "props_are_required");
      return false;
    };

    return true;
  };

  $scope.arePropertiesValuesDefined = function() {

    var inputs = document.querySelectorAll("input"),
      inputsAreFilled = true;

    for (var i = inputs.length - 1; i >= 0; i--) {
      if (inputs[i].value.length <= 2) {
        inputsAreFilled = false;

        this.removeFormElementById(inputs[i].id + "_error");
        this.showErrorMessageByElems(inputs[i].id + "_error", inputs[i], "this_field_is_required");
      } else {
        // I don't know why I can not access the elem from the abstract class behaviour. So...
        this.removeFormElementById(inputs[i].id + "_error");
      }
    }

    return inputsAreFilled;
  };

  $scope.areRequirementsMet = function() {
    return (this.arePropertiesDefined() && this.arePropertiesValuesDefined());
  };

  $scope.highlightPropertiesInDom = function(properties, containerSelector) {
    Object.keys(properties).forEach(function(key) {
      //console.log("highlighting: ", key, properties[key].relativeSelector);
      $scope.highlightPropertyInDom(properties[key].relativeSelector, containerSelector);
    });
  };

  $scope.getElementsSelector = function() {

    return "//body//*[not(contains (@id, 'andes-sidebar'))]";
    //return "//a | //label | //span | //li | //tr | div[not(contains (@id, 'andes-sidebar'))]";
    //return selector + "//span | " + selector + "//a"; //changed for the experiment. Prev value: *
  };

  $scope.adaptPropertyLabelLength = function(str){
    return (str && str.length > 35)? str.substring(0, 35) + "..." : str;
  }

  $scope.onElementSelection = function(data) { //selector exampleValue (will have also a name)

    var prop = {
      "name": "",
      "exampleValue": data.exampleValue,
      "relativeSelector": data.selectors[Object.keys(data.selectors)[0]][0]
    };

    var propControl = this.addPropertyToSidebar(prop);
    propControl.querySelector("input").focus();

    this.removeFormElementById("no_props_error");
  };

  $scope.saveDataModel = function() {
    $scope.service.propertySelectors = $scope.getUserEditedProperties();
    TemplateService.setProperties($scope.service.propertySelectors).then(function() {
      TemplateService.updateServices();
    });
  };

  $scope.getUserEditedProperties = function() {
    var props = {}, propsElems = document.querySelectorAll(".list-group-item");

    for (var i = propsElems.length - 1; i >= 0; i--) {

      var propSelector = propsElems[i].querySelector("button").prop.relativeSelector;
      var propName = propsElems[i].querySelector("input").value;

      props[propName] = propSelector;
    };

    return props;
  };

  $scope.loadPropertiesIntoSidebar = function(properties) {

    Object.keys(properties).forEach(function(i) {
      
      browser.runtime.sendMessage({ //cause it is async, you need to send the prop also
        "call": "getDomTextualValue",
        "args": {
          "relativeSelector": properties[i],
          "name": i,
          "exampleValue": ""
        }
      }).then(function(filledProp){
        $scope.addPropertyToSidebar(filledProp);
      });
    });
  };

  $scope.highlightPropertyInDom = function(relativeSelector, refElemSelector) {

    browser.runtime.sendMessage({
      "call": "selectMatchingElements",
      "args": {
        "selector": relativeSelector,
        "scrapper": "XpathScrapper",
        "refElemSelector": undefined
      }
    });
  };

  $scope.addPropertyToSidebar = function(prop) {

    var property = document.createElement("div");
    property.className = "list-group-item";

    var closebutton = document.createElement("button");
    closebutton.className = "list-item-close-button";
    closebutton.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
    closebutton.prop = prop;
    closebutton.onclick = function() {
      this.parentElement.remove();
    };
    property.appendChild(closebutton);

    var propNameGroup = document.createElement("div");
    propNameGroup.className = "form-group";
    property.appendChild(propNameGroup);

    var propNameLabel = document.createElement("label");
    propNameLabel.innerHTML = browser.i18n.getMessage("property_name");
    propNameGroup.appendChild(propNameLabel);

    var propNameInput = document.createElement("input");
    propNameInput.setAttribute("type", "text");
    propNameInput.className = "form-control resultProperty";
    propNameInput.id = Date.now(); //for the validation
    propNameInput.value = prop.name;
    propNameGroup.appendChild(propNameInput);

    var propValue = document.createElement("i");
    propValue.innerHTML = browser.i18n.getMessage("example_value") + ": " + $scope.adaptPropertyLabelLength(prop.exampleValue); //$scope.listProperties();
    property.appendChild(propValue);

    document.querySelector("#properties").appendChild(property);
    return property;
  };

  $scope.initialize();
});
